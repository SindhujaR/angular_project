// login.component.ts
import { Component } from '@angular/core';
import { AuthService } from '../auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
})
export class LoginComponent {
  credentials: any = {
    email: '',
    password: '',
    platform: 0,
    deviceId: '',
  };

  constructor(private authService: AuthService) {}

  onSubmit() {
    this.authService.login(this.credentials).subscribe(
      (response) => {
        console.log('Login successful:', response);

        // if(response.status == true){
        //   console.log(response.message);
        // }
        // else{
        //   console.log(response.message);

        // }
        // Handle successful login response here
      },
      (error) => {
        console.error('Login error:', error);
        // Handle login error here
      }
    );
  }
}
